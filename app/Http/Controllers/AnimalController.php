<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\animales;
use App\Utils\LogHelper;

class AnimalController extends Controller
{
    public function index()
    {
        try {
            $animales = animales::all();
            return view('animales.index', compact('animales'));
        } catch (\Exception $e) {
            LogHelper::logError($this,$e);
            // Manejar la excepción, en este caso, simplemente devuelve un mensaje de error
            $fechaHoraActual = date("Y-m-d H:i:s");
            $mensaje = $fechaHoraActual . " NO SE PUDO REALIZAR ESTA ACCION";
           
        }
    }

    public function create()
    {
        return view('animales.create');
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'nombre' => 'required|string|max:255',
                'raza' => 'required|string|max:255',
            ]);

            animales::create([
                'nombre' => $request->nombre,
                'raza' => $request->raza,
            ]);

            return redirect()->route('animales.index')->with('msn_success', 'Animal creado exitosamente');
        } catch (\Exception $e) {
            LogHelper::logError($this,$e);
            // Manejar la excepción, en este caso, simplemente devuelve un mensaje de error
            $fechaHoraActual = date("Y-m-d H:i:s");
            $mensaje = $fechaHoraActual . " NO SE PUDO REALIZAR ESTA ACCION";
            return redirect()->route('animales.create')->with('msn_error', $mensaje);
        }
    }
}
