<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaAnimales extends Migration
{
    /**
     * Ejecutar las migraciones.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animales', function (Blueprint $table) {
            $table->id(); // Campo ID como clave primaria
            $table->string('nombre'); // Campo para el nombre del animal
            $table->string('raza'); // Campo para la raza del animal
            $table->timestamps(); // Campos created_at y updated_at
        });
    }

    /**
     * Revertir las migraciones.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animales');
    }
}

