<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('animales')->insert([
            [
                'nombre' => 'Firulais',
                'raza' => 'Labrador',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'nombre' => 'Pelusa',
                'raza' => 'Persa',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'nombre' => 'Max',
                'raza' => 'Pastor Alemán',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'nombre' => 'Bella',
                'raza' => 'Bulldog',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::table('animales')->whereIn('nombre', ['Firulais', 'Pelusa', 'Max', 'Bella'])->delete();
    }
};
