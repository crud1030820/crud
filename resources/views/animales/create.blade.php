@extends('layouts.app')

@section('title','create')

@section('content')

    <div class="container">
        <h1>Crear Nuevo Animal</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('animales.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="nombre">Nombre del Animal:</label>
                <input type="text" name="nombre" id="nombre" class="form-control" value="{{ old('nombre') }}">
            </div>
            <div class="form-group">
                <label for="raza">Raza del Animal:</label>
                <input type="text" name="raza" id="raza" class="form-control" value="{{ old('raza') }}">
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @if(session('msn_error'))

<script>

  let mensaje = "{{ session('msn_error') }}";

  Swal.fire({

    icon: "error",

    html: `<span style="font-size: 16px;">${mensaje}</span>`,

  });

</script>

@endif
    @endsection