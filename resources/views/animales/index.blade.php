@extends('layouts.app')

@section('title','Home')

@section('content')

<div class="container">
    <div class="d-flex mt-3 justify-content-center">
        <h2>Tabla de Peliculas</h2>
    </div>
    <div class="row container m-2">
        <div class="col-12">
            <a href="{{ route('animales.create') }}" class="btn btn-outline-primary">Agregar</a>
        </div>
        <div class="table-responsive rounded m-1">
            <table class="table table-hover">
                <thead class="table-dark">
                    <tr>
                        <th scope="col">N°</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Raza</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($animales as $Animal)
                    <tr>
                        <th>{{ $Animal -> id }}</th>
                        <th>{{ $Animal -> nombre }}</th>
                        <th>{{ $Animal -> raza }}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@if(session('msn_error'))

<script>

  let mensaje = "{{ session('msn_error') }}";

  Swal.fire({

    icon: "error",

    html: `<span style="font-size: 16px;">${mensaje}</span>`,

  });

</script>

@endif

@if(session('msn_success'))

<script>

  let mensaje="{{ session('msn_success') }}";

  Swal.fire({

    icon:"success",

    html: `<span style="font-size: 16px;">${mensaje}</span>`,

  });

</script>


@endif
@endsection