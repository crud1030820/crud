<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnimalController;


Route::get('/', [AnimalController::class, 'index'])->name('animales.index');
Route::get('/animales/create', [AnimalController::class, 'create'])->name('animales.create');
Route::post('/animales/store', [AnimalController::class, 'store'])->name('animales.store');